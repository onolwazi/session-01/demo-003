# demo-003

### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/onolwazi/session-01/demo-003)

### 🖐 To force the rebuild of the image, click on this button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#imagebuild/https://gitlab.com/onolwazi/session-01/demo-003)

```bash
python3 -m http.server
```
